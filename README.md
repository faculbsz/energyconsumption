### Setup requirements

Java 8

Maven

### How to run:

mvn spring-boot:run 

### Swagger Documentation

* This service use Swagger in order to document the whole API.
* Controller methods are not documented  properly, just to save time, but i think a good API should use an API First Approach for example with OpenApi. Also, http status, request and response should be well documented with examples.
* Java classes are not documented because methods names are very explicit, anyway i think a good product should always have Java Docs.

URL = http://localhost:8081/swagger-ui.html

### Usage and models

This API allows you to administrate your profiles. Also you can add diferent meter readings to one profile with differents meter ids.

#### Create new profile using:
http://localhost:8081/profile

Request:

```
{
  "name": "A",
  "fractions": [
    {
      "month": "JAN",
      "value": 0.08
    },
    {
      "month": "FEB",
      "value": 0.08
    },
    {
      "month": "MAR",
      "value": 0.08
    },
    {
      "month": "APR",
      "value": 0.08
    },
    {
      "month": "MAY",
      "value": 0.08
    },
    {
      "month": "JUN",
      "value": 0.08
    },
    {
      "month": "JUL",
      "value": 0.08
    },
    {
      "month": "AUG",
      "value": 0.08
    },
    {
      "month": "SEP",
      "value": 0.08
    },
    {
      "month": "OCT",
      "value": 0.08
    },
    {
      "month": "NOV",
      "value": 0.08
    },
    {
      "month": "DEC",
      "value": 0.12
    }
  ]
}
```

#### Add meter readings to a profile using:
http://localhost:8081/profile/{profileName}/meterReading

Request:

```
{

  "meter_id": "1",
  "readings": [
    {
      "month": "JAN",
      "value": 1
    },
    {
      "month": "FEB",
      "value": 2
    },
    {
      "month": "MAR",
      "value": 3
    },
    {
      "month": "APR",
      "value": 4
    },
    {
      "month": "MAY",
      "value": 5
    },
    {
      "month": "JUN",
      "value": 6
    },
    {
      "month": "JUL",
      "value": 7
    },
    {
      "month": "AUG",
      "value": 8
    },
    {
      "month": "SEP",
      "value": 9
    },
    {
      "month": "OCT",
      "value": 10
    },
    {
      "month": "NOV",
      "value": 11
    },
    {
      "month": "DEC",
      "value": 12.5
    }
  ]
}

```

#### Retrieve Consumption for a given month

You can obtain the consumption for a month indicating the profile name, meter id and month:

GET:
http://localhost:8081/profile/{profileName}/meterReading/{meterId}/month/{month}/consumption

Example: http://localhost:8081/profile/A/meterReading/1/month/DEC/consumption


### Bonus task

POST http://localhost:8081/profile/import
Body: ...src\main\resources\csv

#### Assumptions:
* Files exist.
* Files are valid.
* The user has read and write permissions in the given folder.
* The folder path is absolute.
* Folder path ends without a slash.

### Test coverage

Only service layer is covered by junit tests, pojos are not covered

### Properties

This service use H2 as database

consumption.tolerance=0.25
