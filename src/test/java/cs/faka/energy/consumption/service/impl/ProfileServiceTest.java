package cs.faka.energy.consumption.service.impl;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.TreeSet;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cs.faka.energy.consumption.dto.ConsumptionDTO;
import cs.faka.energy.consumption.dto.FractionDTO;
import cs.faka.energy.consumption.dto.MeterReadingDTO;
import cs.faka.energy.consumption.dto.ProfileDTO;
import cs.faka.energy.consumption.dto.ReadingDTO;
import cs.faka.energy.consumption.entity.FractionEntity;
import cs.faka.energy.consumption.entity.MeterReadingEntity;
import cs.faka.energy.consumption.entity.ProfileEntity;
import cs.faka.energy.consumption.exception.BusinessException;
import cs.faka.energy.consumption.exception.ResourceNotFoundException;
import cs.faka.energy.consumption.repository.IMeterReadingEntityRepository;
import cs.faka.energy.consumption.repository.IProfileEntityRepository;
import cs.faka.energy.consumption.service.IProfileService;
import cs.faka.energy.consumption.service.builder.DTOBuilder;
import cs.faka.energy.consumption.service.builder.EntityBuilder;
import cs.faka.energy.consumption.util.MonthEnum;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ProfileServiceTest.TestConfiguration.class })
public class ProfileServiceTest {

	@Configuration
	static class TestConfiguration {

		@Bean
		public IProfileService ProfileService() {
			return new ProfileService();
		}
	}

	@MockBean
	private EntityBuilder entityBuilder;

	@MockBean
	private DTOBuilder dTOBuilder;

	@MockBean
	private IProfileEntityRepository iProfileRepository;

	@MockBean
	private IMeterReadingEntityRepository iMeterReadingEntityRepository;

	@Autowired
	private IProfileService iProfileService;

	@Test
	public void testCreate() {

		TreeSet<FractionDTO> fractions = new TreeSet<FractionDTO>();
		ProfileDTO<FractionDTO> profile = new ProfileDTO<FractionDTO>("name", fractions);
		ProfileEntity entity = new ProfileEntity("name");

		Mockito.when(iProfileRepository.countByName("name")).thenReturn(0);
		Mockito.when(entityBuilder.buildProfileEntity(profile)).thenReturn(entity);

		iProfileService.create(profile);

		Mockito.verify(iProfileRepository).save(entity);

	}

	@Test(expected = BusinessException.class)
	public void testCreateAlreadyExists() {

		ProfileDTO<FractionDTO> profile = new ProfileDTO<FractionDTO>("name", new TreeSet<FractionDTO>());

		Mockito.when(iProfileRepository.countByName("name")).thenReturn(1);

		iProfileService.create(profile);
	}

	@Test
	public void testDeleteByName() {

		Mockito.when(iProfileRepository.countByName("name")).thenReturn(1);

		iProfileService.deleteByName("name");

		Mockito.verify(iProfileRepository).deleteByName("name");
	}

	@Test(expected = ResourceNotFoundException.class)
	public void testDeleteByNameDoesntExists() {

		Mockito.when(iProfileRepository.countByName("name")).thenReturn(0);

		iProfileService.deleteByName("name");

	}

	@Test(expected = ResourceNotFoundException.class)
	public void testDeleteByNameDoesntExistByName() {

		Mockito.when(iProfileRepository.findByName("name")).thenReturn(Optional.ofNullable(null));

		iProfileService.deleteByName("name");

	}

	@Test
	public void testgetByName() {

		ProfileEntity profileEntity = new ProfileEntity("name");
		Mockito.when(iProfileRepository.findByName("name")).thenReturn(Optional.of(profileEntity));

		TreeSet<FractionDTO> fractions = new TreeSet<FractionDTO>();
		ProfileDTO<FractionDTO> finalProfileDTO = new ProfileDTO<FractionDTO>("name", fractions);

		Mockito.when(dTOBuilder.buildProfileEntityDTO(profileEntity)).thenReturn(finalProfileDTO);

		ProfileDTO<FractionDTO> byName = iProfileService.getByName("name");

		assertEquals(finalProfileDTO.getName(), byName.getName());
		assertEquals(finalProfileDTO.getFractions(), fractions);
	}

	@Test(expected = ResourceNotFoundException.class)
	public void testgetByNameDoesntExist() {

		Mockito.when(iProfileRepository.findByName("name")).thenReturn(Optional.ofNullable(null));

		iProfileService.getByName("name");

	}
	
	@Test(expected = ResourceNotFoundException.class)
	public void testupdateFractionsByProfileNameDoesntExist() {

		Mockito.when(iProfileRepository.findByName("name")).thenReturn(Optional.ofNullable(null));

		iProfileService.updateFractionsByProfileName("name", null);

	}

	@Test
	public void testupdateFractionsByProfileName() {

		ProfileEntity profileEntity = new ProfileEntity("name");
		ArrayList<FractionEntity> fractionsEntity = new ArrayList<FractionEntity>();
		FractionEntity jan = new FractionEntity(MonthEnum.JAN, 1d, profileEntity);
		fractionsEntity.add(jan);
		FractionEntity feb = new FractionEntity(MonthEnum.FEB, 1d, profileEntity);
		fractionsEntity.add(feb);

		profileEntity.setFractions(fractionsEntity);
		Mockito.when(iProfileRepository.findByName("name")).thenReturn(Optional.of(profileEntity));

		TreeSet<FractionDTO> fractions = new TreeSet<FractionDTO>();
		FractionDTO janDTO = new FractionDTO(MonthEnum.JAN, 3d);
		fractions.add(janDTO);
		FractionDTO febDTO = new FractionDTO(MonthEnum.FEB, 4d);
		fractions.add(febDTO);

		iProfileService.updateFractionsByProfileName("name", fractions);

		Mockito.verify(iProfileRepository).save(profileEntity);

		assertEquals(janDTO.getValue(), profileEntity.getFractions().stream()
				.filter(f -> f.getMonth().equals(janDTO.getMonth())).findFirst().get().getValue(), 0);
		assertEquals(febDTO.getValue(), profileEntity.getFractions().stream()
				.filter(f -> f.getMonth().equals(febDTO.getMonth())).findFirst().get().getValue(), 0);

	}
	
	@Test(expected=BusinessException.class)
	public void testcreateMeterReadingByProfileNameDoesntExistEntityAndMeterId() {
		TreeSet<ReadingDTO> readings = new TreeSet<ReadingDTO>();
		ReadingDTO janDTO = new ReadingDTO(MonthEnum.JAN, 1d);
		readings.add(janDTO);
		ReadingDTO febDTO = new ReadingDTO(MonthEnum.FEB, 2d);
		readings.add(febDTO);

		MeterReadingDTO<ReadingDTO> meterReadingDTO = new MeterReadingDTO<ReadingDTO>("meterId", readings);

		ProfileEntity profileEntity = new ProfileEntity("profileName");

		ArrayList<FractionEntity> fractionsEntity = new ArrayList<FractionEntity>();
		FractionEntity jan = new FractionEntity(MonthEnum.JAN, 0.5d, profileEntity);
		fractionsEntity.add(jan);
		FractionEntity feb = new FractionEntity(MonthEnum.FEB, 0.5d, profileEntity);
		fractionsEntity.add(feb);

		profileEntity.setFractions(fractionsEntity);

		Mockito.when(iProfileRepository.findByName("profileName")).thenReturn(Optional.of(profileEntity));

		Mockito.when(iMeterReadingEntityRepository.countByProfileEntityAndMeterId(profileEntity, "meterId"))
				.thenReturn(1);

		MeterReadingEntity meterReadingEntity1 = new MeterReadingEntity();
		MeterReadingEntity meterReadingEntity2 = new MeterReadingEntity();

		List<MeterReadingEntity> collect = new ArrayList<>();
		collect.add(meterReadingEntity1);
		collect.add(meterReadingEntity2);


		iProfileService.createMeterReadingByProfileName(meterReadingDTO, "profileName");

	}

	@Test
	public void testcreateMeterReadingByProfileName() {
		TreeSet<ReadingDTO> readings = new TreeSet<ReadingDTO>();
		ReadingDTO janDTO = new ReadingDTO(MonthEnum.JAN, 1d);
		readings.add(janDTO);
		ReadingDTO febDTO = new ReadingDTO(MonthEnum.FEB, 2d);
		readings.add(febDTO);

		MeterReadingDTO<ReadingDTO> meterReadingDTO = new MeterReadingDTO<ReadingDTO>("meterId", readings);

		ProfileEntity profileEntity = new ProfileEntity("profileName");

		ArrayList<FractionEntity> fractionsEntity = new ArrayList<FractionEntity>();
		FractionEntity jan = new FractionEntity(MonthEnum.JAN, 0.5d, profileEntity);
		fractionsEntity.add(jan);
		FractionEntity feb = new FractionEntity(MonthEnum.FEB, 0.5d, profileEntity);
		fractionsEntity.add(feb);

		profileEntity.setFractions(fractionsEntity);

		Mockito.when(iProfileRepository.findByName("profileName")).thenReturn(Optional.of(profileEntity));

		Mockito.when(iMeterReadingEntityRepository.countByProfileEntityAndMeterId(profileEntity, "meterId"))
				.thenReturn(0);

		MeterReadingEntity meterReadingEntity1 = new MeterReadingEntity();
		MeterReadingEntity meterReadingEntity2 = new MeterReadingEntity();

		List<MeterReadingEntity> collect = new ArrayList<>();
		collect.add(meterReadingEntity1);
		collect.add(meterReadingEntity2);

		Mockito.when(entityBuilder.buildMeterReadingEntity("meterId", janDTO, profileEntity, null))
				.thenReturn(meterReadingEntity1);
		Mockito.when(entityBuilder.buildMeterReadingEntity("meterId", febDTO, profileEntity, meterReadingEntity1))
				.thenReturn(meterReadingEntity2);

		iProfileService.createMeterReadingByProfileName(meterReadingDTO, "profileName");

		Mockito.verify(iMeterReadingEntityRepository).saveAll(collect);

	}

	@Test(expected = ResourceNotFoundException.class)
	public void testcreateMeterReadingByProfileNameProfileDoesntExist() {

		Mockito.when(iProfileRepository.findByName("name")).thenReturn(Optional.ofNullable(null));

		iProfileService.createMeterReadingByProfileName(new MeterReadingDTO<ReadingDTO>(null, null), "profileName");

	}

	@Test
	public void testvalidateMeterReadings() {
		TreeSet<ReadingDTO> readings = new TreeSet<ReadingDTO>();
		ReadingDTO janDTO = new ReadingDTO(MonthEnum.JAN, 1d);
		readings.add(janDTO);
		ReadingDTO febDTO = new ReadingDTO(MonthEnum.FEB, 2d);
		readings.add(febDTO);

		ProfileEntity profileEntity = new ProfileEntity("profileName");

		ArrayList<FractionEntity> fractionsEntity = new ArrayList<FractionEntity>();
		FractionEntity jan = new FractionEntity(MonthEnum.JAN, 0.5d, profileEntity);
		fractionsEntity.add(jan);
		FractionEntity feb = new FractionEntity(MonthEnum.FEB, 0.5d, profileEntity);
		fractionsEntity.add(feb);

		iProfileService.validateMeterReadings(readings, fractionsEntity);

	}

	@Test(expected = BusinessException.class)
	public void testvalidateMeterReadingsNok() {
		TreeSet<ReadingDTO> readings = new TreeSet<ReadingDTO>();
		ReadingDTO janDTO = new ReadingDTO(MonthEnum.JAN, 1d);
		readings.add(janDTO);
		ReadingDTO febDTO = new ReadingDTO(MonthEnum.FEB, 5d);
		readings.add(febDTO);

		ProfileEntity profileEntity = new ProfileEntity("profileName");

		ArrayList<FractionEntity> fractionsEntity = new ArrayList<FractionEntity>();
		FractionEntity jan = new FractionEntity(MonthEnum.JAN, 0.5d, profileEntity);
		fractionsEntity.add(jan);
		FractionEntity feb = new FractionEntity(MonthEnum.FEB, 0.5d, profileEntity);
		fractionsEntity.add(feb);

		iProfileService.validateMeterReadings(readings, fractionsEntity);

	}

	@Test
	public void testdeleteMeterReadingsByMeterIdAndProfileName() {

		ProfileEntity profileEntity = new ProfileEntity("name");
		Mockito.when(iProfileRepository.findByName("name")).thenReturn(Optional.of(profileEntity));

		Mockito.when(iMeterReadingEntityRepository.countByProfileEntityAndMeterId(profileEntity, "meterId"))
				.thenReturn(1);

		iProfileService.deleteMeterReadingsByMeterIdAndProfileName("meterId", "name");

		Mockito.verify(iMeterReadingEntityRepository).deleteByProfileEntityAndMeterId(profileEntity, "meterId");
	}
	
	@Test(expected = ResourceNotFoundException.class)
	public void testdeleteMeterReadingsByMeterIdAndProfileNameNokDoesnExistProfile() {

		Mockito.when(iProfileRepository.findByName("name")).thenReturn(Optional.ofNullable(null));

		iProfileService.deleteMeterReadingsByMeterIdAndProfileName("meterId", "name");
	}

	@Test(expected = ResourceNotFoundException.class)
	public void testdeleteMeterReadingsByMeterIdAndProfileNameNokDoesnExistCombination() {

		ProfileEntity profileEntity = new ProfileEntity("name");
		Mockito.when(iProfileRepository.findByName("name")).thenReturn(Optional.of(profileEntity));

		Mockito.when(iMeterReadingEntityRepository.countByProfileEntityAndMeterId(profileEntity, "meterId"))
				.thenReturn(0);

		iProfileService.deleteMeterReadingsByMeterIdAndProfileName("meterId", "name");
	}

	@Test
	public void testgetMeterReadingsByMeterIdAndProfileName() {
		ProfileEntity profileEntity = new ProfileEntity("name");
		MeterReadingDTO<ReadingDTO> meterReadingDTO = new MeterReadingDTO<ReadingDTO>(null, null);
		ArrayList<MeterReadingEntity> value = new ArrayList<MeterReadingEntity>();

		Mockito.when(iProfileRepository.findByName("name")).thenReturn(Optional.of(profileEntity));

		Mockito.when(iMeterReadingEntityRepository.findByProfileEntityAndMeterId(profileEntity, "meterId"))
				.thenReturn(value);
		Mockito.when(dTOBuilder.buildMeterReadingDTO("meterId", value)).thenReturn(meterReadingDTO);

		assertEquals(iProfileService.getMeterReadingsByMeterIdAndProfileName("meterId", "name"), meterReadingDTO);

	}

	@Test(expected = ResourceNotFoundException.class)
	public void testgetMeterReadingsByMeterIdAndProfileNameProfileDoesntExist() {

		Mockito.when(iProfileRepository.findByName("name")).thenReturn(Optional.ofNullable(null));

		iProfileService.getMeterReadingsByMeterIdAndProfileName("meterId", "name");

	}

	@Test(expected = ResourceNotFoundException.class)
	public void testupdateMeterReadingsByMeterIdAndProfileNameProfileDoesntExist() {

		Mockito.when(iProfileRepository.findByName("name")).thenReturn(Optional.ofNullable(null));

		iProfileService.updateMeterReadingsByMeterIdAndProfileName("meterId", "name", null);

	}

	@Test(expected = ResourceNotFoundException.class)
	public void testupdateMeterReadingsByMeterIdAndProfileNameProfileAndMeterIdDoesntExist() {

		TreeSet<ReadingDTO> readings = new TreeSet<ReadingDTO>();
		ReadingDTO janDTO = new ReadingDTO(MonthEnum.JAN, 1d);
		readings.add(janDTO);
		ReadingDTO febDTO = new ReadingDTO(MonthEnum.FEB, 2d);
		readings.add(febDTO);

		ProfileEntity profileEntity = new ProfileEntity("profileName");

		ArrayList<FractionEntity> fractionsEntity = new ArrayList<FractionEntity>();
		FractionEntity jan = new FractionEntity(MonthEnum.JAN, 0.5d, profileEntity);
		fractionsEntity.add(jan);
		FractionEntity feb = new FractionEntity(MonthEnum.FEB, 0.5d, profileEntity);
		fractionsEntity.add(feb);

		profileEntity.setFractions(fractionsEntity);

		Mockito.when(iProfileRepository.findByName("profileName")).thenReturn(Optional.of(profileEntity));

		Mockito.when(iMeterReadingEntityRepository.countByProfileEntityAndMeterId(profileEntity, "meterId"))
				.thenReturn(0);

		ArrayList<MeterReadingEntity> meterReadingsEntity = new ArrayList<MeterReadingEntity>();
		MeterReadingEntity janReading = new MeterReadingEntity("meterId", MonthEnum.JAN, 2, profileEntity, null);
		meterReadingsEntity.add(janReading);
		MeterReadingEntity febReading = new MeterReadingEntity("meterId", MonthEnum.FEB, 5, profileEntity, janReading);
		meterReadingsEntity.add(febReading);

		Mockito.when(iMeterReadingEntityRepository.findByProfileEntityAndMeterId(profileEntity, "meterId"))
				.thenReturn(meterReadingsEntity);

		iProfileService.updateMeterReadingsByMeterIdAndProfileName("meterId", "profileName", readings);
	

	}

	@Test
	public void testupdateMeterReadingsByMeterIdAndProfileName() {

		TreeSet<ReadingDTO> readings = new TreeSet<ReadingDTO>();
		ReadingDTO janDTO = new ReadingDTO(MonthEnum.JAN, 1d);
		readings.add(janDTO);
		ReadingDTO febDTO = new ReadingDTO(MonthEnum.FEB, 2d);
		readings.add(febDTO);

		ProfileEntity profileEntity = new ProfileEntity("profileName");

		ArrayList<FractionEntity> fractionsEntity = new ArrayList<FractionEntity>();
		FractionEntity jan = new FractionEntity(MonthEnum.JAN, 0.5d, profileEntity);
		fractionsEntity.add(jan);
		FractionEntity feb = new FractionEntity(MonthEnum.FEB, 0.5d, profileEntity);
		fractionsEntity.add(feb);

		profileEntity.setFractions(fractionsEntity);

		Mockito.when(iProfileRepository.findByName("profileName")).thenReturn(Optional.of(profileEntity));

		Mockito.when(iMeterReadingEntityRepository.countByProfileEntityAndMeterId(profileEntity, "meterId"))
				.thenReturn(1);

		ArrayList<MeterReadingEntity> meterReadingsEntity = new ArrayList<MeterReadingEntity>();
		MeterReadingEntity janReading = new MeterReadingEntity("meterId", MonthEnum.JAN, 2, profileEntity, null);
		meterReadingsEntity.add(janReading);
		MeterReadingEntity febReading = new MeterReadingEntity("meterId", MonthEnum.FEB, 5, profileEntity, janReading);
		meterReadingsEntity.add(febReading);

		Mockito.when(iMeterReadingEntityRepository.findByProfileEntityAndMeterId(profileEntity, "meterId"))
				.thenReturn(meterReadingsEntity);

		iProfileService.updateMeterReadingsByMeterIdAndProfileName("meterId", "profileName", readings);

		assertEquals(janDTO.getValue(), janReading.getReading(), 0);
		assertEquals(febDTO.getValue(), febReading.getReading(), 0);
	}

	@Test(expected = ResourceNotFoundException.class)
	public void testgetConsumptionByMeterIdAndProfileNameAndMonthProfileDoesntExist() {

		Mockito.when(iProfileRepository.findByName("name")).thenReturn(Optional.ofNullable(null));

		iProfileService.getConsumptionByMeterIdAndProfileNameAndMonth("meterId", "name", null);

	}

	@Test(expected = ResourceNotFoundException.class)
	public void testgetConsumptionByMeterIdAndProfileNameAndMonthDoesntExistProfileAndMeterIdAndMonth() {

		ProfileEntity entity = new ProfileEntity();

		Mockito.when(iProfileRepository.findByName("name")).thenReturn(Optional.ofNullable(entity));

		Mockito.when(iMeterReadingEntityRepository.findByProfileEntityAndMeterIdAndMonth(entity, "meterId", null))
				.thenReturn(Optional.ofNullable(null));

		iProfileService.getConsumptionByMeterIdAndProfileNameAndMonth("meterId", "name", null);

	}

	@Test
	public void testgetConsumptionByMeterIdAndProfileNameAndMonth() {

		ProfileEntity profileEntity = new ProfileEntity("profileName");

		ArrayList<MeterReadingEntity> meterReadingsEntity = new ArrayList<MeterReadingEntity>();
		MeterReadingEntity jan = new MeterReadingEntity("meterId", MonthEnum.JAN, 2, profileEntity, null);
		meterReadingsEntity.add(jan);
		MeterReadingEntity feb = new MeterReadingEntity("meterId", MonthEnum.FEB, 5, profileEntity, jan);
		meterReadingsEntity.add(feb);

		profileEntity.setMeterReading(meterReadingsEntity);

		Mockito.when(iProfileRepository.findByName("profileName")).thenReturn(Optional.of(profileEntity));

		Mockito.when(iMeterReadingEntityRepository.countByProfileEntityAndMeterId(profileEntity, "meterId"))
				.thenReturn(1);

		Mockito.when(iMeterReadingEntityRepository.findByProfileEntityAndMeterIdAndMonth(profileEntity, "meterId",
				MonthEnum.FEB)).thenReturn(Optional.of(feb));

		ConsumptionDTO consumptionByMeterIdAndProfileNameAndMonth = iProfileService
				.getConsumptionByMeterIdAndProfileNameAndMonth("meterId", "profileName", MonthEnum.FEB);

		assertEquals(3, consumptionByMeterIdAndProfileNameAndMonth.getValue(), 0);
	}

	@Test
	public void testgetConsumptionByMeterIdAndProfileNameAndMonthWithoutPreviosMonth() {

		ProfileEntity profileEntity = new ProfileEntity("profileName");

		ArrayList<MeterReadingEntity> meterReadingsEntity = new ArrayList<MeterReadingEntity>();
		MeterReadingEntity jan = new MeterReadingEntity("meterId", MonthEnum.JAN, 1, profileEntity, null);
		meterReadingsEntity.add(jan);
		MeterReadingEntity feb = new MeterReadingEntity("meterId", MonthEnum.FEB, 2, profileEntity, jan);
		meterReadingsEntity.add(feb);

		profileEntity.setMeterReading(meterReadingsEntity);

		Mockito.when(iProfileRepository.findByName("profileName")).thenReturn(Optional.of(profileEntity));

		Mockito.when(iMeterReadingEntityRepository.countByProfileEntityAndMeterId(profileEntity, "meterId"))
				.thenReturn(1);

		Mockito.when(iMeterReadingEntityRepository.findByProfileEntityAndMeterIdAndMonth(profileEntity, "meterId",
				MonthEnum.JAN)).thenReturn(Optional.of(jan));

		ConsumptionDTO consumptionByMeterIdAndProfileNameAndMonth = iProfileService
				.getConsumptionByMeterIdAndProfileNameAndMonth("meterId", "profileName", MonthEnum.JAN);

		assertEquals(1, consumptionByMeterIdAndProfileNameAndMonth.getValue(), 0);
	}

}
