package cs.faka.energy.consumption.service.impl;

import java.io.File;
import java.util.TreeSet;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cs.faka.energy.consumption.controller.validator.impl.ProfileValidator;
import cs.faka.energy.consumption.dto.FractionDTO;
import cs.faka.energy.consumption.dto.IValuePerMonthDTO;
import cs.faka.energy.consumption.dto.MeterReadingDTO;
import cs.faka.energy.consumption.dto.ProfileDTO;
import cs.faka.energy.consumption.dto.ReadingDTO;
import cs.faka.energy.consumption.service.IProfileFilesConsumer;
import cs.faka.energy.consumption.service.IProfileService;
import cs.faka.energy.consumption.util.FileHandler;
import cs.faka.energy.consumption.util.MonthEnum;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { ProfileFilesConsumerTest.TestConfiguration.class })
public class ProfileFilesConsumerTest {

	@Configuration
	static class TestConfiguration {

		@Bean
		public IProfileFilesConsumer iProfileFilesConsumer() {
			return new ProfileFilesConsumer();
		}
	}

	@MockBean
	private FileHandler fileHandler;

	@MockBean
	private ProfileValidator profileValidator;

	@MockBean
	private IProfileService iProfileService;

	@Autowired
	private IProfileFilesConsumer iProfileFilesConsumer;

	private static ProfileDTO<IValuePerMonthDTO> profileA;

	private static ProfileDTO<IValuePerMonthDTO> profileB;

	private static MeterReadingDTO<IValuePerMonthDTO> meterReadingA;

	private static MeterReadingDTO<IValuePerMonthDTO> meterReadingB;

	private static TreeSet<IValuePerMonthDTO> treeSetA;

	private static TreeSet<IValuePerMonthDTO> treeSetB;

	private static TreeSet<IValuePerMonthDTO> fractionsA;

	private static TreeSet<IValuePerMonthDTO> fractionsB;
	
	@BeforeClass
	public static void setup() {
		
		fractionsA = new TreeSet<IValuePerMonthDTO>();

		fractionsA.add(new FractionDTO(MonthEnum.JAN, 0.08));
		fractionsA.add(new FractionDTO(MonthEnum.FEB, 0.08));
		fractionsA.add(new FractionDTO(MonthEnum.MAR, 0.08));
		fractionsA.add(new FractionDTO(MonthEnum.APR, 0.08));
		fractionsA.add(new FractionDTO(MonthEnum.MAY, 0.08));
		fractionsA.add(new FractionDTO(MonthEnum.JUN, 0.08));
		fractionsA.add(new FractionDTO(MonthEnum.JUL, 0.08));
		fractionsA.add(new FractionDTO(MonthEnum.AUG, 0.08));
		fractionsA.add(new FractionDTO(MonthEnum.SEP, 0.08));
		fractionsA.add(new FractionDTO(MonthEnum.OCT, 0.08));
		fractionsA.add(new FractionDTO(MonthEnum.NOV, 0.08));
		fractionsA.add(new FractionDTO(MonthEnum.DEC, 0.12));

		fractionsB = new TreeSet<IValuePerMonthDTO>();

		fractionsB.add(new FractionDTO(MonthEnum.JAN, 0.08));
		fractionsB.add(new FractionDTO(MonthEnum.FEB, 0.08));
		fractionsB.add(new FractionDTO(MonthEnum.MAR, 0.08));
		fractionsB.add(new FractionDTO(MonthEnum.APR, 0.08));
		fractionsB.add(new FractionDTO(MonthEnum.MAY, 0.08));
		fractionsB.add(new FractionDTO(MonthEnum.JUN, 0.08));
		fractionsB.add(new FractionDTO(MonthEnum.JUL, 0.08));
		fractionsB.add(new FractionDTO(MonthEnum.AUG, 0.08));
		fractionsB.add(new FractionDTO(MonthEnum.SEP, 0.08));
		fractionsB.add(new FractionDTO(MonthEnum.OCT, 0.08));
		fractionsB.add(new FractionDTO(MonthEnum.NOV, 0.08));
		fractionsB.add(new FractionDTO(MonthEnum.DEC, 0.12));

		profileA = new ProfileDTO<IValuePerMonthDTO>("A", fractionsA);
		profileB = new ProfileDTO<IValuePerMonthDTO>("B", fractionsB);

		treeSetA = new TreeSet<IValuePerMonthDTO>();
		
		treeSetA.add(new ReadingDTO(MonthEnum.JAN, 1));
		treeSetA.add(new ReadingDTO(MonthEnum.FEB, 2));
		treeSetA.add(new ReadingDTO(MonthEnum.MAR, 3));
		treeSetA.add(new ReadingDTO(MonthEnum.APR, 4));
		treeSetA.add(new ReadingDTO(MonthEnum.MAY, 5));
		treeSetA.add(new ReadingDTO(MonthEnum.JUN, 6));
		treeSetA.add(new ReadingDTO(MonthEnum.JUL, 7));
		treeSetA.add(new ReadingDTO(MonthEnum.AUG, 8));
		treeSetA.add(new ReadingDTO(MonthEnum.SEP, 9));
		treeSetA.add(new ReadingDTO(MonthEnum.OCT, 10));
		treeSetA.add(new ReadingDTO(MonthEnum.NOV, 11));
		treeSetA.add(new ReadingDTO(MonthEnum.DEC, 12.5));

		treeSetB = new TreeSet<IValuePerMonthDTO>();

		treeSetB.add(new ReadingDTO(MonthEnum.JAN, 1));
		treeSetB.add(new ReadingDTO(MonthEnum.FEB, 2));
		treeSetB.add(new ReadingDTO(MonthEnum.MAR, 3));
		treeSetB.add(new ReadingDTO(MonthEnum.APR, 4));
		treeSetB.add(new ReadingDTO(MonthEnum.MAY, 5));
		treeSetB.add(new ReadingDTO(MonthEnum.JUN, 6));
		treeSetB.add(new ReadingDTO(MonthEnum.JUL, 7));
		treeSetB.add(new ReadingDTO(MonthEnum.AUG, 8));
		treeSetB.add(new ReadingDTO(MonthEnum.SEP, 9));
		treeSetB.add(new ReadingDTO(MonthEnum.OCT, 10));
		treeSetB.add(new ReadingDTO(MonthEnum.NOV, 11));
		treeSetB.add(new ReadingDTO(MonthEnum.DEC, 12.5));

		meterReadingA = new MeterReadingDTO<IValuePerMonthDTO>("0001", treeSetA);
		meterReadingB = new MeterReadingDTO<IValuePerMonthDTO>("0004", treeSetB);
	}

	@Test
	public void testReadProfilesFromFile() {

		String absolutePath = new File("src/test/resources/csv").getAbsolutePath();
		iProfileFilesConsumer.readProfilesFromFile(absolutePath);

		Mockito.verify(iProfileService).create(profileA);
		Mockito.verify(iProfileService).create(profileB);
		
		Mockito.verify(iProfileService).createMeterReadingByProfileName(meterReadingA, "A");
		Mockito.verify(iProfileService).createMeterReadingByProfileName(meterReadingB, "B");

		Mockito.verify(fileHandler).removeFiles(absolutePath, "profiles.csv", "meterReadings.csv");

	}

	@Test
	public void testReadProfilesFromFileReadingsInvalid() {

		
		Mockito.doThrow(new RuntimeException("message")).when(profileValidator).validateReadings(Mockito.eq(meterReadingA.getReadings()));
		
		String absolutePath = new File("src/test/resources/csv").getAbsolutePath();
		iProfileFilesConsumer.readProfilesFromFile(absolutePath);


		Mockito.verify(fileHandler).writeFiles(Mockito.eq(absolutePath), Mockito.any());

	}

	@Test
	public void testReadProfilesFromFileFractionsInvalid() {

		Mockito.doThrow(new RuntimeException("message")).when(profileValidator).validateFractions(Mockito.eq(fractionsA));
		
		String absolutePath = new File("src/test/resources/csv").getAbsolutePath();
		iProfileFilesConsumer.readProfilesFromFile(absolutePath);


		Mockito.verify(fileHandler).writeFiles(Mockito.eq(absolutePath), Mockito.any());



	}

}
