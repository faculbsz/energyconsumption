package cs.faka.energy.consumption.controller.validator;

import static org.junit.Assert.assertTrue;

import java.util.TreeSet;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import cs.faka.energy.consumption.controller.validator.impl.ProfileValidator;
import cs.faka.energy.consumption.dto.FractionDTO;
import cs.faka.energy.consumption.exception.BusinessException;
import cs.faka.energy.consumption.util.MonthEnum;

@RunWith(JUnit4.class)
public class ProfileValidatorTest {

	@Test
	public void testValidateFractionsOk() {
		TreeSet<FractionDTO> fractions = new TreeSet<FractionDTO>();
		fractions.add(new FractionDTO(MonthEnum.FEB, 0.5d));
		fractions.add(new FractionDTO(MonthEnum.JAN, 0.5d));
		new ProfileValidator().validateFractions(fractions);

		assertTrue(true);
	}

	@Test(expected = BusinessException.class)
	public void testValidateFractionsNok() {
		TreeSet<FractionDTO> fractions = new TreeSet<FractionDTO>();
		fractions.add(new FractionDTO(MonthEnum.FEB, 0.5d));
		fractions.add(new FractionDTO(MonthEnum.JAN, 0.6d));
		new ProfileValidator().validateFractions(fractions);
	}

}
