package cs.faka.energy.consumption.repository;
import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import cs.faka.energy.consumption.entity.MeterReadingEntity;
import cs.faka.energy.consumption.entity.ProfileEntity;
import cs.faka.energy.consumption.util.MonthEnum;

public interface IMeterReadingEntityRepository extends CrudRepository<MeterReadingEntity, Long>{

	void deleteByProfileEntityAndMeterId(ProfileEntity profile, String meterId);

	List<MeterReadingEntity> findByProfileEntityAndMeterId(ProfileEntity profile, String meterId);

	int countByProfileEntityAndMeterId(ProfileEntity profile, String meterId);

	Optional<MeterReadingEntity> findByProfileEntityAndMeterIdAndMonth(ProfileEntity profile, String meterId,
			MonthEnum month);

}