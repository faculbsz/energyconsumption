package cs.faka.energy.consumption.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import cs.faka.energy.consumption.dto.ProfileDTO;
import cs.faka.energy.consumption.entity.ProfileEntity;

public interface IProfileEntityRepository extends CrudRepository<ProfileEntity,Long>{

	int countByName(String name);

	void deleteByName(String profileName);

	Optional<ProfileEntity> findByName(String profileName);

}
