package cs.faka.energy.consumption.util;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class FileHandler {

	public void removeFiles(String pathFile, String profilesFileName, String meterReadingsFileName) {
		new File(pathFile + "\\" + profilesFileName).delete();
		new File(pathFile + "\\" + meterReadingsFileName).delete();
	}

	public void writeFiles(String pathFile, List<String> logs) {
		try {
			Files.write(Paths.get(pathFile), logs, StandardCharsets.UTF_8);
		} catch (IOException e) {
			throw new RuntimeException(
					"An exception ocurred while writing errors log file, content: " + String.join("\n", logs), e);
		}
	}

}
