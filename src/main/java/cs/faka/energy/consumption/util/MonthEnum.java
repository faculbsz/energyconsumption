package cs.faka.energy.consumption.util;

public enum MonthEnum {

	JAN, FEB, MAR, APR, MAY, JUN, JUL, AUG, SEP, OCT, NOV, DEC;

	public static MonthEnum getByName(String name) {
		for (MonthEnum e : values()) {
			if (e.name().equals(name))
				return e;
		}
		throw new IllegalArgumentException("Unexpected month name value.");
	}

}
