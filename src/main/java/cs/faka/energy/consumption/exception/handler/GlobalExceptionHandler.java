package cs.faka.energy.consumption.exception.handler;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import cs.faka.energy.consumption.exception.BusinessException;
import cs.faka.energy.consumption.exception.CommunicationException;
import cs.faka.energy.consumption.exception.ResourceNotFoundException;
import cs.faka.energy.consumption.exception.model.ErrorInfo;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;

/**
 * This handler is not covered by unit tests, i already use it in most of my projects. If unit tests for this class are needed tell me, and ill update them to the repository asap.
 * @author Facundo
 *
 */
@RestControllerAdvice
@RestController
public class GlobalExceptionHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class);
	private static final String ERR_MSG_FIELD_MALFORMED = "Format error in field: '%s'";
	private static final String ERR_MSG_FIELD_WRONG_DATA_TYPE = "The format of '%s' is incorrect. Expected type: '%s' and received '%s'";
	private static final String ERR_MSG_WRONG_DATA_TYPE = "The format is incorrect. Expected type: '%s' and received '%s'"; 

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(ServletRequestBindingException.class)
	public @ResponseBody ErrorInfo handleBinding(HttpServletRequest req, ServletRequestBindingException ex) {
		LOGGER.info("ServletRequestBindingException", ex);
		return new ErrorInfo("400", ex.getMessage());
	}
	
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(Exception.class)
	public @ResponseBody ErrorInfo handleBinding(HttpServletRequest req, Exception ex) {
		LOGGER.error("TechnicalException", ex);
		return new ErrorInfo("500", ex.getMessage());
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(BusinessException.class)
	public @ResponseBody ErrorInfo handlerBusinessException(HttpServletRequest req, BusinessException ex) {
		LOGGER.error(ex.getMessage(), ex);
		String message = ex.getMessage();
		if (ex.getMessage() == null && ex.getCode() == null && ex.getCause() != null) {
			message = ex.getCause().getMessage();
		}
		LOGGER.info("BusinessException", ex);
		return new ErrorInfo(ex.getCode() != null ? ex.getCode().toString() : "400", message);
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public @ResponseBody ErrorInfo handlerValidRequestException(MethodArgumentNotValidException ex) {
		LOGGER.info("MethodArgumentNotValidException", ex);
		return new ErrorInfo("400", ex.getBindingResult().getFieldError().getDefaultMessage());
	}
	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(BindException.class)
	public @ResponseBody ErrorInfo handlerValidRequestException(BindException ex) {
		LOGGER.info("BindException", ex);
		return new ErrorInfo("400", ex.getBindingResult().getFieldError().getDefaultMessage());
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(HttpMessageNotReadableException.class)
	@ResponseBody
	public ErrorInfo handlerValidRequestException(HttpMessageNotReadableException ex) {
		LOGGER.info("HttpMessageNotReadableException", ex);
		String message = getNotRedeableMessage(ex);
		return new ErrorInfo("400", message);
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(value = { TypeMismatchException.class, InvalidFormatException.class })
	public @ResponseBody ErrorInfo handleBadRequest(Exception ex) {
		LOGGER.error(ex.getMessage(), ex);
		String message = null;

		if (ex instanceof TypeMismatchException) {
			TypeMismatchException tme = (TypeMismatchException) ex;
			if (tme.getPropertyName() != null) {
				message = String.format(ERR_MSG_FIELD_WRONG_DATA_TYPE,tme.getPropertyName(),tme.getRequiredType().getSimpleName(),tme.getValue());
			} else {
				message = String.format(ERR_MSG_WRONG_DATA_TYPE,tme.getRequiredType().getSimpleName(),tme.getValue());
			}
		} else if (ex instanceof HttpMessageNotReadableException) {
			message = getNotRedeableMessage((HttpMessageNotReadableException) ex);
		} else {
			message = ex.getMessage();
		}
		LOGGER.info("TypeMismatchException.class, InvalidFormatException.class", ex);
		return new ErrorInfo("400", message);
	}

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(CommunicationException.class)
	public @ResponseBody ErrorInfo handlerCommunicationException(HttpServletRequest req, CommunicationException ex) {
		LOGGER.error(ex.getMessage(), ex);
		return new ErrorInfo("500", ex.getMessage());
	}

	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler(ResourceNotFoundException.class)
	public @ResponseBody ErrorInfo handlerResourceNotFoundException(ResourceNotFoundException ex) {
		LOGGER.error(ex.getMessage(), ex);
		return new ErrorInfo(String.valueOf(HttpStatus.NOT_FOUND.value()), ex.getMessage());
	}

	private String getNotRedeableMessage(HttpMessageNotReadableException ex) {
		String message = ex.getMostSpecificCause().getMessage();
		if (ex.getCause() instanceof com.fasterxml.jackson.core.JsonParseException) {
			try {
				JsonParseException exj = ((JsonParseException) ex.getCause());
				message = String.format(ERR_MSG_FIELD_MALFORMED, exj.getProcessor().getCurrentName());
			} catch (IOException e) {
				LOGGER.info("IOException", e);
			}
		} else if (ex.getMostSpecificCause() instanceof BusinessException) {
			message = ex.getMostSpecificCause().getMessage();
		} else if (ex.getCause() instanceof InvalidFormatException) {
			InvalidFormatException ife = (InvalidFormatException) ex.getCause();
			message = String.format(ERR_MSG_FIELD_WRONG_DATA_TYPE, ife.getPath().get(0).getFieldName(),
					ife.getTargetType().getSimpleName(), ife.getValue());
		} else {
			message = ex.getMessage();
		}
		return message;
	}

}
