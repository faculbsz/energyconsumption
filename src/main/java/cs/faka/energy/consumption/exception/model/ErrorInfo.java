package cs.faka.energy.consumption.exception.model;

import io.swagger.annotations.ApiModelProperty;

public class ErrorInfo {

	@ApiModelProperty("Internal error code")
	public final String errorCode;
	@ApiModelProperty("Message error")
	public final String errorMessage;

	public ErrorInfo(String errorCode, String errorMessage) {
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}

}
