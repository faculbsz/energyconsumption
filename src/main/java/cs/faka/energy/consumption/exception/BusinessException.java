package cs.faka.energy.consumption.exception;

public class BusinessException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	private Integer code;

	public BusinessException(String message, Integer code) {
		super(message);
		this.code = code;
	}

	public BusinessException(String message) {
		super(message);
	}

	public BusinessException(String message, Throwable e) {
		super(message, e);
	}

	public BusinessException(){};
	
	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
	
}
