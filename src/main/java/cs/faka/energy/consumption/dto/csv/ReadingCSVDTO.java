package cs.faka.energy.consumption.dto.csv;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import cs.faka.energy.consumption.dto.IValuePerMonthDTO;
import cs.faka.energy.consumption.util.MonthEnum;

@JsonPropertyOrder({ "MeterID", "Profile", "Month", "Meter reading" })
public class ReadingCSVDTO implements IValuePerMonthDTO {

	@JsonProperty("MeterID")
	private String meterId;

	@JsonProperty("Profile")
	private String profile;

	private double value;

	private MonthEnum month;

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public String getMeterId() {
		return meterId;
	}

	public void setMeterId(String meterId) {
		this.meterId = meterId;
	}

	@Override
	@JsonProperty("Month")
	public void setMonth(MonthEnum month) {
		this.month = month;
	}

	@Override
	@JsonProperty("Month")
	public MonthEnum getMonth() {
		return this.month;
	}

	@Override
	@JsonProperty("Fraction")
	public void setValue(double value) {
		this.value = value;
	}

	@Override
	@JsonProperty("Fraction")
	public double getValue() {
		return this.value;
	}

	@Override
	public String toString() {
		return "ReadingCSVDTO [getProfile()=" + getProfile() + ", getMeterId()=" + getMeterId() + ", getMonth()="
				+ getMonth() + ", getValue()=" + getValue() + "]";
	}

}
