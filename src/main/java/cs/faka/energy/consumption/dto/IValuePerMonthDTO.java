package cs.faka.energy.consumption.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public interface IValuePerMonthDTO extends IMonthComparable{

	@JsonProperty("value")
	public double getValue();

	@JsonProperty("value")
	public void setValue(double value);

}
