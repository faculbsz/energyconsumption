package cs.faka.energy.consumption.dto;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import com.fasterxml.jackson.annotation.JsonProperty;

import cs.faka.energy.consumption.util.MonthEnum;

public interface IMonthComparable extends Comparable<IMonthComparable> {

	@JsonProperty("month")
	public MonthEnum getMonth();

	@JsonProperty("month")
	public void setMonth(MonthEnum month);

	SimpleDateFormat fmt = new SimpleDateFormat("MMM", Locale.US);

	@Override
	default int compareTo(IMonthComparable o) {
		try {
			return fmt.parse(this.getMonth().name()).compareTo(fmt.parse(o.getMonth().name()));
		} catch (ParseException e) {
			e.printStackTrace();
			return this.compareTo(o);
		}
	}

}
