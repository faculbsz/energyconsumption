package cs.faka.energy.consumption.dto;

import java.util.TreeSet;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "name", "fractions" })
public class ProfileDTO <E extends IValuePerMonthDTO>{

	@JsonProperty("name")
	@NotEmpty
	private String name;
	
	@JsonProperty("fractions")
	@NotNull
	@Valid
	private TreeSet<E> fractions = null;
	
	public ProfileDTO(@NotEmpty String name, @NotNull @Valid TreeSet<E> fractions) {
		super();
		this.name = name;
		this.fractions = fractions;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("fractions")
	public TreeSet<E> getFractions() {
		return fractions;
	}

	@JsonProperty("fractions")
	public void setFractions(TreeSet<E> fractions) {
		this.fractions = fractions;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fractions == null) ? 0 : fractions.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProfileDTO other = (ProfileDTO) obj;
		if (fractions == null) {
			if (other.fractions != null)
				return false;
		} else if (!fractions.equals(other.fractions))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	

}