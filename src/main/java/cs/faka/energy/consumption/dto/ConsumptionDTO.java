package cs.faka.energy.consumption.dto;

import com.fasterxml.jackson.annotation.JsonProperty;


public class ConsumptionDTO {
	
	@JsonProperty("value")
	private double value;
	
	public ConsumptionDTO(double value) {
		this.value = value;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}
	
	
	
}
