package cs.faka.energy.consumption.dto;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import cs.faka.energy.consumption.util.MonthEnum;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "month", "value" })
public class ReadingDTO implements IValuePerMonthDTO{

	@JsonProperty("month")
	@NotNull
	private MonthEnum month;

	@JsonProperty("value")
	@NotNull
	private double value;
	
	public ReadingDTO() {
	}

	public ReadingDTO(MonthEnum month, @NotNull double value) {
		super();
		this.month = month;
		this.value = value;
	}

	@JsonProperty("month")
	public MonthEnum getMonth() {
		return month;
	}

	@JsonProperty("month")
	public void setMonth(MonthEnum month) {
		this.month = month;
	}

	@JsonProperty("value")
	public double getValue() {
		return value;
	}

	@JsonProperty("value")
	public void setValue(double value) {
		this.value = value;
	}

}