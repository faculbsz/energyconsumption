package cs.faka.energy.consumption.dto;

import java.util.TreeSet;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "meter_id", "readings" })
public class MeterReadingDTO <E extends IValuePerMonthDTO>{

	@JsonProperty("meter_id")
	@NotEmpty
	private String meterId;
	
	@JsonProperty("readings")
	@Valid
	private TreeSet<E> readings;
	
	public MeterReadingDTO(@NotEmpty String meterId, @Valid TreeSet<E> readings) {
		super();
		this.meterId = meterId;
		this.readings = readings;
	}

	@JsonProperty("meter_id")
	public String getMeterId() {
		return meterId;
	}

	@JsonProperty("meter_id")
	public void setMeterId(String meterId) {
		this.meterId = meterId;
	}

	@JsonProperty("readings")
	public TreeSet<E> getReadings() {
		return readings;
	}

	@JsonProperty("readings")
	public void setReadings(TreeSet<E> readings) {
		this.readings = readings;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((meterId == null) ? 0 : meterId.hashCode());
		result = prime * result + ((readings == null) ? 0 : readings.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MeterReadingDTO other = (MeterReadingDTO) obj;
		if (meterId == null) {
			if (other.meterId != null)
				return false;
		} else if (!meterId.equals(other.meterId))
			return false;
		if (readings == null) {
			if (other.readings != null)
				return false;
		} else if (!readings.equals(other.readings))
			return false;
		return true;
	}
	
	

}
