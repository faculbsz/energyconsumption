package cs.faka.energy.consumption.service;

import java.util.List;
import java.util.TreeSet;

import javax.validation.Valid;

import cs.faka.energy.consumption.dto.ConsumptionDTO;
import cs.faka.energy.consumption.dto.FractionDTO;
import cs.faka.energy.consumption.dto.IValuePerMonthDTO;
import cs.faka.energy.consumption.dto.MeterReadingDTO;
import cs.faka.energy.consumption.dto.ProfileDTO;
import cs.faka.energy.consumption.dto.ReadingDTO;
import cs.faka.energy.consumption.entity.FractionEntity;
import cs.faka.energy.consumption.util.MonthEnum;

public interface IProfileService {

	<E extends IValuePerMonthDTO> void create(ProfileDTO<E> profile);

	void deleteByName(String profileName);

	ProfileDTO<FractionDTO> getByName(String profileName);

	<E extends IValuePerMonthDTO> void updateFractionsByProfileName(String profileName, TreeSet<E> fractions);

	<E extends IValuePerMonthDTO> void createMeterReadingByProfileName(MeterReadingDTO<E> meterReading,
			String profileName);

	void deleteMeterReadingsByMeterIdAndProfileName(String meterId, String profileName);

	MeterReadingDTO<ReadingDTO> getMeterReadingsByMeterIdAndProfileName(String meterId, String profileName);

	<E extends IValuePerMonthDTO> void updateMeterReadingsByMeterIdAndProfileName(String meterId, String profileName,
			@Valid TreeSet<E> readings);

	<E extends IValuePerMonthDTO> void validateMeterReadings(TreeSet<E> readings, List<FractionEntity> fractions);

	ConsumptionDTO getConsumptionByMeterIdAndProfileNameAndMonth(String meterId, String profileName, MonthEnum month);

}
