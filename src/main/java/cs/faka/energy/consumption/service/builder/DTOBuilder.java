package cs.faka.energy.consumption.service.builder;

import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import cs.faka.energy.consumption.dto.FractionDTO;
import cs.faka.energy.consumption.dto.MeterReadingDTO;
import cs.faka.energy.consumption.dto.ProfileDTO;
import cs.faka.energy.consumption.dto.ReadingDTO;
import cs.faka.energy.consumption.entity.FractionEntity;
import cs.faka.energy.consumption.entity.MeterReadingEntity;
import cs.faka.energy.consumption.entity.ProfileEntity;

@Service
public class DTOBuilder {

	public ProfileDTO<FractionDTO> buildProfileEntityDTO(ProfileEntity profile) {
		TreeSet<FractionDTO> fractions = profile.getFractions().stream().map(this::buildFractionDTO)
				.collect(Collectors.toCollection(TreeSet::new));

		return new ProfileDTO<FractionDTO>(profile.getName(), fractions);
	}

	public FractionDTO buildFractionDTO(FractionEntity fraction) {

		return new FractionDTO(fraction.getMonth(), fraction.getValue());
	}

	public MeterReadingDTO<ReadingDTO> buildMeterReadingDTO(String meterId, List<MeterReadingEntity> readings) {

		return new MeterReadingDTO<ReadingDTO>(meterId, readings.stream().map(this::buildReadingDTO).collect(Collectors.toCollection(TreeSet::new)));

	}

	public ReadingDTO buildReadingDTO(MeterReadingEntity meterReadingEntity) {

		return new ReadingDTO(meterReadingEntity.getMonth(), meterReadingEntity.getReading());

	}

}
