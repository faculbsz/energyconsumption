package cs.faka.energy.consumption.service;

public interface IProfileFilesConsumer {

	void readProfilesFromFile(String pathFile);
	
}
