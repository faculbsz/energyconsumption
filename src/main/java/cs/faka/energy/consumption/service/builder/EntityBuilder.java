package cs.faka.energy.consumption.service.builder;

import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import cs.faka.energy.consumption.dto.IValuePerMonthDTO;
import cs.faka.energy.consumption.dto.ProfileDTO;
import cs.faka.energy.consumption.entity.FractionEntity;
import cs.faka.energy.consumption.entity.MeterReadingEntity;
import cs.faka.energy.consumption.entity.ProfileEntity;

@Service
public class EntityBuilder {
	
	
	public <E extends IValuePerMonthDTO> ProfileEntity buildProfileEntity(ProfileDTO<E> profile) {
		ProfileEntity profileEntity = new ProfileEntity(profile.getName());
		
		profileEntity.setFractions(profile.getFractions().stream().map(f -> this.buildFractionEntity(f, profileEntity)).collect(Collectors.toList()));
			
		return profileEntity;
	}
	
	public FractionEntity buildFractionEntity(IValuePerMonthDTO fraction, ProfileEntity profileEntity) {
		
		return new FractionEntity(fraction.getMonth(), fraction.getValue(), profileEntity);
	}
	
	public MeterReadingEntity buildMeterReadingEntity(String meterId, IValuePerMonthDTO reading, ProfileEntity profileEntity, MeterReadingEntity meterReadingEntity) {
		
		return new MeterReadingEntity(meterId, reading.getMonth(), reading.getValue(), profileEntity, meterReadingEntity);
		
	}

}
