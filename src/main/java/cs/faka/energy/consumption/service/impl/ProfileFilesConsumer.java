package cs.faka.energy.consumption.service.impl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import cs.faka.energy.consumption.controller.validator.impl.ProfileValidator;
import cs.faka.energy.consumption.dto.IValuePerMonthDTO;
import cs.faka.energy.consumption.dto.MeterReadingDTO;
import cs.faka.energy.consumption.dto.ProfileDTO;
import cs.faka.energy.consumption.dto.csv.FractionCSVDTO;
import cs.faka.energy.consumption.dto.csv.ReadingCSVDTO;
import cs.faka.energy.consumption.service.IProfileFilesConsumer;
import cs.faka.energy.consumption.service.IProfileService;
import cs.faka.energy.consumption.util.FileHandler;

/**
 * 
 * This service will be the responsible of import old-format CSV and will take
 * the following assumptions:
 * 
 * 1. Files names are always the same. 2. In the given path will be a file for
 * Profile and other for Meter Readings. 3. Processed correctly means: all
 * profiles and all meter readings were correctly created.
 * 
 * @author Facundo-Zenbook
 *
 */
@Service
public class ProfileFilesConsumer implements IProfileFilesConsumer {
	

	private String profilesFileName = "profiles.csv";
	private String meterReadingsFileName = "meterReadings.csv";

	@Autowired
	private ProfileValidator profileValidator;

	@Autowired
	private IProfileService iProfileService;
	
	@Autowired
	private FileHandler fileHandler;

	CsvMapper csvMapper = new CsvMapper();
	
	@Override
	public void readProfilesFromFile(String pathFile) {
		List<String> logs = new ArrayList<String>();

		List<String> profilesToOmmit = extractAndCreateProfiles(pathFile, logs);

		extractAndCreateMeterReadings(pathFile, logs, profilesToOmmit);
		
		
		if(logs.isEmpty()) {
			fileHandler.removeFiles(pathFile, profilesFileName, meterReadingsFileName);
		}else {
			fileHandler.writeFiles(pathFile, logs);
		}
		
	}

	private void extractAndCreateMeterReadings(String pathFile, List<String> logs, List<String> profilesOmmited) {
		CsvSchema csvSchema2 = csvMapper.typedSchemaFor(ReadingCSVDTO.class).withHeader();
		try {
			MappingIterator<ReadingCSVDTO> readValues = new CsvMapper().readerFor(ReadingCSVDTO.class)
					.with(csvSchema2.withColumnSeparator(CsvSchema.DEFAULT_COLUMN_SEPARATOR))
					.readValues(new File(pathFile + "\\" + meterReadingsFileName));
			List<ReadingCSVDTO> profilesAndReadings = readValues.readAll();

			Map<String, List<ReadingCSVDTO>> collect = profilesAndReadings.stream()
					.collect(Collectors.groupingBy(ReadingCSVDTO::getProfile));
			
			//I remove profiles with errors
			collect.keySet().removeAll(profilesOmmited);

			for (String profileName : collect.keySet()) {

				List<ReadingCSVDTO> readings = collect.get(profileName);

				Map<String, TreeSet<IValuePerMonthDTO>> readingsForOneProfileAndByMeterId = readings.stream().collect(
						Collectors.groupingBy(ReadingCSVDTO::getMeterId, Collectors.toCollection(TreeSet::new)));

				for (String meterId : readingsForOneProfileAndByMeterId.keySet()) {

					try {
						TreeSet<IValuePerMonthDTO> readingsByMeterIdAndProfile = readingsForOneProfileAndByMeterId
								.get(meterId);

						profileValidator.validateReadings(readingsByMeterIdAndProfile);

						iProfileService.createMeterReadingByProfileName(
								new MeterReadingDTO<IValuePerMonthDTO>(meterId, readingsByMeterIdAndProfile), profileName);


					} catch (RuntimeException e) {
						logs.add(new StringBuilder("Meter Reading NOT CREATED for Profile ").append(profileName).append(" and meterId ").append(meterId).append(" ERROR ").append(e.getMessage()).toString());
					}
				}

			}
		} catch (IOException e) {
			throw new RuntimeException("An exception ocurred while parsing profiles csv", e);
		}
	}

	private List<String> extractAndCreateProfiles(String pathFile, List<String> logs) {
		CsvSchema csvSchema = csvMapper.typedSchemaFor(FractionCSVDTO.class).withHeader();
		List<String> profilesOmmited = new ArrayList<String>();

		try {
			MappingIterator<FractionCSVDTO> readValues = new CsvMapper().readerWithTypedSchemaFor(FractionCSVDTO.class)
					.with(csvSchema.withColumnSeparator(CsvSchema.DEFAULT_COLUMN_SEPARATOR))
					.readValues(new File(pathFile + "\\" + profilesFileName));

			List<FractionCSVDTO> profilesAndFractions = readValues.readAll();

			Map<String, TreeSet<IValuePerMonthDTO>> collect = profilesAndFractions.stream()
					.collect(Collectors.groupingBy(FractionCSVDTO::getProfile, Collectors.toCollection(TreeSet::new)));

			for (String profileName : collect.keySet()) {

				try {
					TreeSet<IValuePerMonthDTO> fractions = collect.get(profileName);
					profileValidator.validateFractions(fractions);

					iProfileService.create(new ProfileDTO<IValuePerMonthDTO>(profileName, fractions));

				} catch (RuntimeException e) {
					profilesOmmited.add(profileName);
					logs.add(new StringBuilder("PROFILE ").append(profileName).append(" Error: ").append(e.getMessage()).toString());
				}

			}

		} catch (IOException e) {			
			throw new RuntimeException("An exception ocurred while parsing profiles csv", e);
		}
		return profilesOmmited;
	}

}
