package cs.faka.energy.consumption.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import cs.faka.energy.consumption.dto.ConsumptionDTO;
import cs.faka.energy.consumption.dto.FractionDTO;
import cs.faka.energy.consumption.dto.IValuePerMonthDTO;
import cs.faka.energy.consumption.dto.MeterReadingDTO;
import cs.faka.energy.consumption.dto.ProfileDTO;
import cs.faka.energy.consumption.dto.ReadingDTO;
import cs.faka.energy.consumption.entity.FractionEntity;
import cs.faka.energy.consumption.entity.MeterReadingEntity;
import cs.faka.energy.consumption.entity.ProfileEntity;
import cs.faka.energy.consumption.exception.BusinessException;
import cs.faka.energy.consumption.exception.ResourceNotFoundException;
import cs.faka.energy.consumption.repository.IMeterReadingEntityRepository;
import cs.faka.energy.consumption.repository.IProfileEntityRepository;
import cs.faka.energy.consumption.service.IProfileService;
import cs.faka.energy.consumption.service.builder.DTOBuilder;
import cs.faka.energy.consumption.service.builder.EntityBuilder;
import cs.faka.energy.consumption.util.MonthEnum;

@Service
public class ProfileService implements IProfileService {

	private static final String DOESNT_EXIST_A_CONSUMPTION_IN_MONTH_S_FOR_GIVE_PROFILE_NAME_S_AND_METER_ID_S = "Doesnt exist a consumption in month %s for give profileName %s and meterId %s";

	private static final String ALREADY_EXISTS_A_METER_READINGS_FOR_PROFILE_S_AND_METER_ID_S = "Already exists a meter readings for profile %s and meterId %s";

	private static final String DOESNT_EXISTS_A_METER_WITH_PROFILE_AND_METERID = "Doesnt exists a meter with profile %s and meterId %s";

	private static final String CONSUMPTION_FOR_MONTH_S_IS_OUT_OF_THE_RANGE = "Consumption for month %s is out of the range";

	private static final String DOESNT_EXIST_A_PROFILE_WITH_GIVEN_NAME_S = "Doesnt exist a profile with given name %s";

	private static final String ALREADY_EXISTS_A_PROFILE_WITH_NAME_S = "Already exists a profile with name %s";

	@Autowired
	private IProfileEntityRepository iProfileRepository;

	@Autowired
	private IMeterReadingEntityRepository iMeterReadingEntityRepository;

	@Autowired
	private EntityBuilder entityBuilder;

	@Autowired
	private DTOBuilder dTOBuilder;

	@Value("${consumption.tolerance:0.25}")
	private double tolerance;

	@Override
	public <E extends IValuePerMonthDTO> void create(ProfileDTO<E> profile) {

		if (iProfileRepository.countByName(profile.getName()) > 0) {
			throw new BusinessException(String.format(ALREADY_EXISTS_A_PROFILE_WITH_NAME_S, profile.getName()));
		}

		iProfileRepository.save(entityBuilder.buildProfileEntity(profile));
	}

	@Override
	public void deleteByName(String profileName) {
		if (iProfileRepository.countByName(profileName) == 0) {
			throw new ResourceNotFoundException(String.format(DOESNT_EXIST_A_PROFILE_WITH_GIVEN_NAME_S, profileName));
		}

		iProfileRepository.deleteByName(profileName);

	}

	@Override
	public ProfileDTO<FractionDTO> getByName(String profileName) {
		return iProfileRepository.findByName(profileName).map(dTOBuilder::buildProfileEntityDTO)
				.orElseThrow(() -> new ResourceNotFoundException(
						String.format(DOESNT_EXIST_A_PROFILE_WITH_GIVEN_NAME_S, profileName)));

	}

	@Override
	public <E extends IValuePerMonthDTO> void updateFractionsByProfileName(String profileName, TreeSet<E> fractions) {

		ProfileEntity profile = iProfileRepository.findByName(profileName)
				.orElseThrow(() -> new ResourceNotFoundException(
						String.format(DOESNT_EXIST_A_PROFILE_WITH_GIVEN_NAME_S, profileName)));

		profile.getFractions().forEach(f -> {

			for (IValuePerMonthDTO fractionDTO : fractions) {
				if (f.getMonth().equals(fractionDTO.getMonth())) {
					f.setValue(fractionDTO.getValue());
				}
			}
		});

		iProfileRepository.save(profile);
	}

	@Override
	public <E extends IValuePerMonthDTO> void createMeterReadingByProfileName(MeterReadingDTO<E> meterReading,
			String profileName) {

		ProfileEntity profile = iProfileRepository.findByName(profileName)
				.orElseThrow(() -> new ResourceNotFoundException(
						String.format(DOESNT_EXIST_A_PROFILE_WITH_GIVEN_NAME_S, profileName)));

		validateMeterReadings(meterReading.getReadings(), profile.getFractions());

		if (iMeterReadingEntityRepository.countByProfileEntityAndMeterId(profile, meterReading.getMeterId()) > 0) {
			throw new BusinessException(String.format(ALREADY_EXISTS_A_METER_READINGS_FOR_PROFILE_S_AND_METER_ID_S,
					profileName, meterReading.getMeterId()));
		}

		List<MeterReadingEntity> collect = new ArrayList<>();
		MeterReadingEntity previous = null;

		for (IValuePerMonthDTO readingDTO : meterReading.getReadings()) {

			MeterReadingEntity buildMeterReadingEntity = entityBuilder
					.buildMeterReadingEntity(meterReading.getMeterId(), readingDTO, profile, previous);
			collect.add(buildMeterReadingEntity);
			previous = buildMeterReadingEntity;
		}
		iMeterReadingEntityRepository.saveAll(collect);

	}

	@Override
	public <E extends IValuePerMonthDTO> void validateMeterReadings(TreeSet<E> readings,
			List<FractionEntity> fractions) {

		// Assuming that the input of readings is valid, and also the total consumption
		// is the consumption of December because there is no reading from previouse
		// December.
		double totalConsumptionPerYear = readings.last().getValue();

		Map<MonthEnum, Double> fractionsMapPerMonth = fractions.stream()
				.collect(Collectors.toMap(x -> x.getMonth(), x -> x.getValue()));

		IValuePerMonthDTO previous = null;
		for (IValuePerMonthDTO reading : readings) {
			if (previous != null) {
				double consumption = reading.getValue() - previous.getValue();
				double percentByMonth = fractionsMapPerMonth.get(reading.getMonth());

				double averageConsumption = percentByMonth * totalConsumptionPerYear;

				double toleranceByMonth = averageConsumption * tolerance;

				if ((consumption > (averageConsumption + toleranceByMonth))
						|| consumption < (averageConsumption - toleranceByMonth)) {
					throw new BusinessException(
							String.format(CONSUMPTION_FOR_MONTH_S_IS_OUT_OF_THE_RANGE, reading.getMonth()));
				}

			}
			previous = reading;
		}

	}

	@Override
	public void deleteMeterReadingsByMeterIdAndProfileName(String meterId, String profileName) {

		ProfileEntity profile = iProfileRepository.findByName(profileName)
				.orElseThrow(() -> new ResourceNotFoundException(
						String.format(DOESNT_EXIST_A_PROFILE_WITH_GIVEN_NAME_S, profileName)));

		if (iMeterReadingEntityRepository.countByProfileEntityAndMeterId(profile, meterId) == 0) {
			throw new ResourceNotFoundException(
					String.format(DOESNT_EXISTS_A_METER_WITH_PROFILE_AND_METERID, profileName, meterId));
		}

		iMeterReadingEntityRepository.deleteByProfileEntityAndMeterId(profile, meterId);

	}

	@Override
	public MeterReadingDTO<ReadingDTO> getMeterReadingsByMeterIdAndProfileName(String meterId, String profileName) {

		ProfileEntity profile = iProfileRepository.findByName(profileName)
				.orElseThrow(() -> new ResourceNotFoundException(
						String.format(DOESNT_EXIST_A_PROFILE_WITH_GIVEN_NAME_S, profileName)));

		return dTOBuilder.buildMeterReadingDTO(meterId,
				iMeterReadingEntityRepository.findByProfileEntityAndMeterId(profile, meterId));

	}

	@Override
	public <E extends IValuePerMonthDTO> void updateMeterReadingsByMeterIdAndProfileName(String meterId,
			String profileName, @Valid TreeSet<E> readings) {

		ProfileEntity profile = iProfileRepository.findByName(profileName)
				.orElseThrow(() -> new ResourceNotFoundException(
						String.format(DOESNT_EXIST_A_PROFILE_WITH_GIVEN_NAME_S, profileName)));

		validateMeterReadings(readings, profile.getFractions());

		if (iMeterReadingEntityRepository.countByProfileEntityAndMeterId(profile, meterId) == 0) {
			throw new ResourceNotFoundException(
					String.format(DOESNT_EXISTS_A_METER_WITH_PROFILE_AND_METERID, profileName, meterId));
		}

		List<MeterReadingEntity> meterReadings = iMeterReadingEntityRepository.findByProfileEntityAndMeterId(profile,
				meterId);

		meterReadings.forEach(r -> {

			for (IValuePerMonthDTO reading : readings) {
				if (reading.getMonth().equals(r.getMonth())) {
					r.setReading(reading.getValue());
					break;
				}
			}

		});

		iMeterReadingEntityRepository.saveAll(meterReadings);
	}

	@Override
	public ConsumptionDTO getConsumptionByMeterIdAndProfileNameAndMonth(String meterId, String profileName,
			MonthEnum month) {

		ProfileEntity profile = iProfileRepository.findByName(profileName)
				.orElseThrow(() -> new ResourceNotFoundException(
						String.format(DOESNT_EXIST_A_PROFILE_WITH_GIVEN_NAME_S, profileName)));

		MeterReadingEntity meterReading = iMeterReadingEntityRepository
				.findByProfileEntityAndMeterIdAndMonth(profile, meterId, month)
				.orElseThrow(() -> new ResourceNotFoundException(
						String.format(DOESNT_EXIST_A_CONSUMPTION_IN_MONTH_S_FOR_GIVE_PROFILE_NAME_S_AND_METER_ID_S,
								month, profileName, meterId)));

		if (meterReading.getPrevious() == null) {
			return new ConsumptionDTO(meterReading.getReading());
		}

		return new ConsumptionDTO(meterReading.getReading() - meterReading.getPrevious().getReading());
	}

}
