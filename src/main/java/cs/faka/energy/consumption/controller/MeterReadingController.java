package cs.faka.energy.consumption.controller;

import java.util.TreeSet;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cs.faka.energy.consumption.controller.validator.IProfileValidator;
import cs.faka.energy.consumption.dto.ConsumptionDTO;
import cs.faka.energy.consumption.dto.MeterReadingDTO;
import cs.faka.energy.consumption.dto.ReadingDTO;
import cs.faka.energy.consumption.service.IProfileService;
import cs.faka.energy.consumption.util.MonthEnum;

@RestController
@RequestMapping(path = "/profile/{profileName}/meterReading")
public class MeterReadingController {

	private static final String APPLICATION_JSON = "application/json";
	
	@Autowired
	private IProfileService iProfileService;
	
	@Autowired
	private IProfileValidator iProfileValidator;

	@PostMapping(consumes = APPLICATION_JSON)
	@Transactional
	public ResponseEntity<Void> createMeterReading(@PathVariable String profileName, @RequestBody @Valid MeterReadingDTO<ReadingDTO> meterReading) {
		iProfileValidator.validateReadings(meterReading.getReadings());
		
		iProfileService.createMeterReadingByProfileName(meterReading, profileName);
		
		return ResponseEntity.ok().build();
	}
	
	@DeleteMapping(path= "/{meterId}")
	@Transactional
	public ResponseEntity<Void> deleteMeterReading(@PathVariable String profileName, @PathVariable String meterId) {
		
		iProfileService.deleteMeterReadingsByMeterIdAndProfileName(meterId, profileName);
		
		return ResponseEntity.ok().build();
	}
	
	@GetMapping(path= "/{meterId}", produces = APPLICATION_JSON)
	@Transactional(readOnly = true)
	public ResponseEntity<MeterReadingDTO<ReadingDTO>> getMeterReadings(@PathVariable String profileName, @PathVariable String meterId) {
		
		return ResponseEntity.ok(iProfileService.getMeterReadingsByMeterIdAndProfileName(meterId, profileName));
	}
	
	@GetMapping(path= "/{meterId}/month/{month}/consumption", produces = APPLICATION_JSON)
	@Transactional(readOnly = true)
	public ResponseEntity<ConsumptionDTO> getConsumption(@PathVariable String profileName, @PathVariable String meterId,  @PathVariable MonthEnum month) {
		
		return ResponseEntity.ok(iProfileService.getConsumptionByMeterIdAndProfileNameAndMonth(meterId, profileName, month));
	}
	
	@PutMapping(path= "/{meterId}/readings", consumes = APPLICATION_JSON)
	@Transactional
	public ResponseEntity<Void> updateMeterReadings(@PathVariable String profileName, @RequestBody @Valid TreeSet<ReadingDTO> readings, @PathVariable String meterId) {
		iProfileValidator.validateReadings(readings);
		
		iProfileService.updateMeterReadingsByMeterIdAndProfileName(meterId, profileName, readings);
		
		return ResponseEntity.ok().build();
	}

}
