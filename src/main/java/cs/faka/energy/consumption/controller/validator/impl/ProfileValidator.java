package cs.faka.energy.consumption.controller.validator.impl;

import java.util.TreeSet;

import org.springframework.stereotype.Service;

import cs.faka.energy.consumption.controller.validator.IProfileValidator;
import cs.faka.energy.consumption.dto.IValuePerMonthDTO;
import cs.faka.energy.consumption.exception.BusinessException;

@Service
public class ProfileValidator implements IProfileValidator {

	@Override
	public <E extends IValuePerMonthDTO> void validateFractions(TreeSet<E> fractions) {

		if (fractions.stream().mapToDouble(IValuePerMonthDTO::getValue).sum() != 1) {
			throw new BusinessException("Fractions sum is not 1.");
		}
	}

	@Override
	public <E extends IValuePerMonthDTO> void validateReadings(TreeSet<E> readings) {

		readings.forEach(r -> {
			IValuePerMonthDTO lower = readings.lower(r);
			// lower will be null for JAN
			if (lower != null) {
				if (r.getValue() < lower.getValue()) {
					throw new BusinessException(
							String.format("Reading from %s cannot be lower than reading from month %", r.getMonth(),
									lower.getMonth()));
				}
			}
		});

	}

}
