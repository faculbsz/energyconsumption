package cs.faka.energy.consumption.controller.validator;

import java.util.TreeSet;

import cs.faka.energy.consumption.dto.IValuePerMonthDTO;

/**
 * 
 * This Validator will only validate inputs with other inputs, all validations
 * which need an acces to the database will be grouped in his respective
 * Service.
 * 
 * @author Facundo
 *
 */
public interface IProfileValidator {

	public <E extends IValuePerMonthDTO> void validateFractions(TreeSet<E> fractions);

	public <E extends IValuePerMonthDTO> void validateReadings(TreeSet<E> fractions);

}
