package cs.faka.energy.consumption.controller;

import java.util.TreeSet;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cs.faka.energy.consumption.controller.validator.IProfileValidator;
import cs.faka.energy.consumption.dto.FractionDTO;
import cs.faka.energy.consumption.dto.IValuePerMonthDTO;
import cs.faka.energy.consumption.dto.ProfileDTO;
import cs.faka.energy.consumption.service.IProfileFilesConsumer;
import cs.faka.energy.consumption.service.IProfileService;

@RestController
@RequestMapping(path = "/profile")
public class ProfileController {

	private static final String APPLICATION_JSON = "application/json";

	@Autowired
	private IProfileService iProfileService;

	@Autowired
	private IProfileValidator iProfileValidator;
	
	@Autowired
	private IProfileFilesConsumer iProfileFilesConsumer;

	@PostMapping(consumes = APPLICATION_JSON)
	@Transactional
	public ResponseEntity<Void> createProfile(@RequestBody @Valid ProfileDTO<FractionDTO> profile) {
		iProfileValidator.validateFractions(profile.getFractions());
		iProfileService.create(profile);

		return ResponseEntity.ok().build();
	}

	@DeleteMapping(path = "/{profileName}")
	@Transactional
	public ResponseEntity<Void> deleteProfile(@PathVariable String profileName) {

		iProfileService.deleteByName(profileName);

		return ResponseEntity.ok().build();
	}

	@GetMapping(path = "/{profileName}", produces = APPLICATION_JSON)
	@Transactional(readOnly = true)
	public ResponseEntity<ProfileDTO<FractionDTO>> getProfile(@PathVariable String profileName) {

		return ResponseEntity.ok(iProfileService.getByName(profileName));
	}

	@PutMapping(path = "/{profileName}/fraction", consumes = APPLICATION_JSON)
	@Transactional
	public ResponseEntity<Void> updateProfileFractions(@PathVariable String profileName,
			@RequestBody @Valid TreeSet<FractionDTO> fractions) {
		iProfileValidator.validateFractions(fractions);
		iProfileService.updateFractionsByProfileName(profileName, fractions);

		return ResponseEntity.ok().build();
	}
	
	@PostMapping("/import")
	public ResponseEntity<Void> importFromFolder(@RequestBody String pathFolder){
		
		iProfileFilesConsumer.readProfilesFromFile(pathFolder);
		
		return ResponseEntity.ok().build();
	}

}
