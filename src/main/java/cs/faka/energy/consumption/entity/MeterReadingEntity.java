package cs.faka.energy.consumption.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import cs.faka.energy.consumption.util.MonthEnum;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"profileEntity.id","month","meterId"}))
public class MeterReadingEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(nullable = false)
	private String meterId;

	@ManyToOne
	@JoinColumn(name = "profileEntity.id", nullable = false)
	private ProfileEntity profileEntity;

	@Column(nullable = false)
    @Enumerated(EnumType.STRING)
	private MonthEnum month;

	@Column(nullable = false)
	private double reading;
	
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private MeterReadingEntity previous;
	
	public MeterReadingEntity() {
	}
	
	public MeterReadingEntity(String meterId, MonthEnum month, double reading, ProfileEntity profileEntity, MeterReadingEntity previous) {
		super();
		this.meterId = meterId;
		this.month = month;
		this.reading = reading;
		this.profileEntity = profileEntity;
		this.previous = previous;
	}
	
	public MeterReadingEntity getPrevious() {
		return previous;
	}

	public void setPrevious(MeterReadingEntity previous) {
		this.previous = previous;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMeterId() {
		return meterId;
	}

	public void setMeterId(String meterId) {
		this.meterId = meterId;
	}

	public ProfileEntity getProfileEntity() {
		return profileEntity;
	}

	public void setProfileEntity(ProfileEntity profileEntity) {
		this.profileEntity = profileEntity;
	}

	public MonthEnum getMonth() {
		return month;
	}

	public void setMonth(MonthEnum month) {
		this.month = month;
	}

	public double getReading() {
		return reading;
	}

	public void setReading(double reading) {
		this.reading = reading;
	}

}
