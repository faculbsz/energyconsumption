package cs.faka.energy.consumption.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class ProfileEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(nullable = false, unique = true)
	private String name;

	@OneToMany(mappedBy = "profileEntity", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<FractionEntity> fractions = new ArrayList<>();

	@OneToMany(mappedBy = "profileEntity", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<MeterReadingEntity> meterReading = new ArrayList<>();
	
	public ProfileEntity() {
	}
	
	public ProfileEntity(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<FractionEntity> getFractions() {
		return fractions;
	}

	public void setFractions(List<FractionEntity> fractions) {
		this.fractions = fractions;
	}

	public List<MeterReadingEntity> getMeterReading() {
		return meterReading;
	}

	public void setMeterReading(List<MeterReadingEntity> meterReading) {
		this.meterReading = meterReading;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
