package cs.faka.energy.consumption.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import cs.faka.energy.consumption.util.MonthEnum;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"profileEntity.id","month"}))
public class FractionEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "profileEntity.id", nullable = false)
	private ProfileEntity profileEntity;

	@Column(nullable = false)
    @Enumerated(EnumType.STRING)
	private MonthEnum month;
	
	@Column(nullable = false)
	private double value;
	
	public FractionEntity() {
	}
	
	public FractionEntity(MonthEnum month, double value, ProfileEntity profileEntity) {
		super();
		this.month = month;
		this.value = value;
		this.profileEntity = profileEntity;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ProfileEntity getProfileEntity() {
		return profileEntity;
	}

	public void setProfileEntity(ProfileEntity profileEntity) {
		this.profileEntity = profileEntity;
	}

	public MonthEnum getMonth() {
		return month;
	}

	public void setMonth(MonthEnum month) {
		this.month = month;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}
	
	

}
